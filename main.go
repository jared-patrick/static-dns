package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"

	"github.com/mdlayher/ethernet"
	"github.com/mdlayher/raw"
)

const etherType = 0xc3b5 // 50101

var (
	configFile = os.Getenv("CONFIG_FILE")
)

type Config struct {
	Interface string `json:"interface"`
}

func newConfig(path string) *Config {
	configFile, err := os.Open(configFile)
	if err != nil {
		log.Fatalf("error opening config file: %s", err)
	}
	configData, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.Fatalf("error reading confg file: %s", err)
	}
	config := &Config{}
	if err := json.Unmarshal(configData, config); err != nil {
		log.Fatal("error reading config json: %s", err)
	}
	return config
}

func main() {
	config := newConfig(configFile)
	netIface, err := net.InterfaceByName(config.Interface)
	if err != nil {
		log.Fatalf("failed to find interface %s %s", config.Interface, err)
	}
	conn, err := raw.ListenPacket(netIface, etherType, nil)
	if err != nil {
		log.Fatalf("failed creating listener: %s", err)
	}
	msg := "Hello World!"

	go sendMessages(conn, netIface.HardwareAddr, msg)
	go receiveMessages(conn, netIface.MTU)

	select {}
}

func sendMessages(c net.PacketConn, source net.HardwareAddr, msg string) {
	f := &ethernet.Frame{
		Destination: net.HardwareAddr{0x08, 0x00, 0x27, 0x01, 0x03, 0x55},
		Source:      source,
		EtherType:   etherType,
		Payload:     []byte(msg),
	}

	b, err := f.MarshalBinary()
	if err != nil {
		log.Fatal("failed to marshal ethernet frame: %s", err)
	}
	addr := &raw.Addr{
		HardwareAddr: ethernet.Broadcast,
	}

	t := time.NewTicker(1 * time.Second)
	for range t.C {
		log.Println("sending message:", msg)
		if _, err := c.WriteTo(b, addr); err != nil {
			log.Fatalf("failed to seend message: %s", err)
		}
	}
}

func receiveMessages(c net.PacketConn, mtu int) {
	var f ethernet.Frame
	b := make([]byte, mtu)

	for {
		log.Println("waiting for messages...")
		n, addr, err := c.ReadFrom(b)
		if err != nil {
			log.Fatal("failed to receive message: %b", err)
		}

		if err := (&f).UnmarshalBinary(b[:n]); err != nil {
			log.Fatalf("failled to unmarshal ethernet frame: %b", err)
		}

		log.Printf("[%s] %s", addr.String(), string(f.Payload))
	}
}
